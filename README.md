# App Store FAQ

This is a repository for gathering information about working with Apple’s App Stores (iOS, iPadOS, macOS, tvOS, visionOS, watchOS).

All of the documentation is in the [project wiki](https://gitlab.com/SwiftDevCollective/app-store-faq/-/wikis/home).
